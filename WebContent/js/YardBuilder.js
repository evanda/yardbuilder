/**
 * 
 */

var allSeries = [];
var activeSeries;

function drawStripes(series) {
	console.log("Striping!");
	console.log(series);
	
	var s1A = series.getPath().getAt(0);
	var s1B = series.getPath().getAt(1);
	var s2B = series.getPath().getAt(2);
	var s2A = series.getPath().getAt(3);
	
	var slots = series.getPath().nSlots;
	
	var deltaAx = (s2A.lat() - s1A.lat()) / slots;
	var deltaAy = (s2A.lng() - s1A.lng()) / slots;
	var deltaBx = (s2B.lat() - s1B.lat()) / slots;
	var deltaBy = (s2B.lng() - s1B.lng()) / slots;

	var zoneCoords = [ s1A, // top left
	s1B, // bot left
	s2B, // bot right
	s2A, // top right
	s1A ];
	
	var slotLines = [];
	for (var i = 1; i < slots; i++) { // for 40 spots we draw 39 lines -- 1-39.
		var Ax = s1A.lat() + deltaAx * i;
		var Ay = s1A.lng() + deltaAy * i;
		var Bx = s1B.lat() + deltaBx * i;
		var By = s1B.lng() + deltaBy * i;

		var slotLineCoords = [ new google.maps.LatLng(Ax, Ay),
				new google.maps.LatLng(Bx, By) ];

		var slotLine = new google.maps.Polyline({
			path : slotLineCoords,
			geodesic : true,
			strokeColor : '#0000FF',
			strokeOpacity : 1.0,
			strokeWeight : 1
		});

		slotLine.setMap(map);
		slotLines.push(slotLine);
	}
	series.getPath().slotLines = slotLines;
}

function clearStripes(series) {
	var slotLines = series.getPath().slotLines;
	for ( var i = 0; i< slotLines.length ; i++ ) {
		slotLines[i].setMap(null);
	}
	series.getPath().slotLines = [];
}

function makeSeries(s1A, s1B, s2A, s2B, slots) {

	var zoneCoords = [ s1A, // top left
	s1B, // bot left
	s2B, // bot right
	s2A ];

	var series = new google.maps.Polygon({
		fillColor : "#FF0000",
		fillOpacity : 0.3,
		path : zoneCoords,
		geodesic : true,
		strokeColor : '#FF0000',
		strokeOpacity : 1.0,
		strokeWeight : 2,
		editable : true
	});
	series.setMap(map);

	google.maps.event.addListener(series.getPath(), 'set_at', function(event) {
		console.log('Bounds changed.');
		clearStripes(this.series); //we sneaked the series object (a polygon) onto the path.  pretty circular!
		drawStripes(this.series);
		activateSeries(this.series);
	});
	
	google.maps.event.addListener(series,'click', function(event) {
		console.log("Click!");
		activateSeries(this);
	});

	series.getPath().series = series;
	series.getPath().nSlots = slots;
	series.getPath().slotLines = [];
	
	return series;
}

function activateSeries(series) {
	resetStriper();
	activeSeries = series;
	showStripingOptions();
	console.log('Showing striping options.');
}

function placeMarker(position, map) {
	
	activeSeries = null;

	var marker = new google.maps.Marker({
		position : position,
		map : map,
		draggable : true
	});

	zoneMarkers.push(marker);
	$("#points").append(
			"<div class='point'><b>" + zoneMarkers.length + "</b>: " + position.toString() + "</div>");

	console.log("Position " + zoneMarkers.length + " is " + position);

	$("#instructions:visible").removeAttr("style").fadeOut();
	switch (zoneMarkers.length) {
	case 1:
		$("#instructions").html(
				"Now choose the \"lower left\" of the zone range.");
		break;
	case 2:
		$("#instructions").html(
				"Now choose the \"top right\" of the zone range.");
		break;
	case 3:
		$("#instructions").html(
				"Now choose the \"lower right\" of the zone range.");
		break;
	case 4:
		showStripingOptions();
		break;
	default:
		console.log("Something is broken!");
		resetStriper();
		break;
	}
	
	$("#instructions").show("drop");

}


function showStripingOptions() {
	//$("points:visible").removeAttr("style").fadeOut();
	$("#instructions").html("How many slots are in that range?");
	$("#striper").show("drop");
}

function resetStriper() {
	$("#instructions").html(
			"<p>Please click the \"top left\" of a range of slots.</p>").show(
			"drop");
	$("#striper").hide();
	for (var i = 0; i < zoneMarkers.length; i++) {
		zoneMarkers[i].setMap(null);
	}
	zoneMarkers = []; // next!
	$("#points").empty();
	$("#striper").hide();
}

function stripeSeries() {
	var nSlots = $("#nSlots").val();

	if ( activeSeries ) {
		console.log("Changing number of slots in series to "+nSlots);
		console.log("Active series: ");
		console.log(activeSeries);
		
		activeSeries.getPath().nSlots = nSlots;
		clearStripes(activeSeries);
		drawStripes(activeSeries);
		resetStriper();
	} else if (zoneMarkers.length == 4) {
		
		console.log("Filling zone with " + nSlots + " slots.");
		
		var series = makeSeries(zoneMarkers[0].position, zoneMarkers[1].position,
				zoneMarkers[2].position, zoneMarkers[3].position, nSlots);

		drawStripes(series);
		resetStriper();
		
		allSeries.push(series);
		
		$("#seriesInfo").html(allSeries.length+" series defined.");

	} else {
		alert("Your code is terrible!");
	}
}